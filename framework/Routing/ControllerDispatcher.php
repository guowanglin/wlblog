<?php
declare(strict_types=1);

namespace Gwl\Framework\Routing;

use Gwl\Framework\Foundation\Application;
use ReflectionMethod;
use RuntimeException;

class ControllerDispatcher
{
    /*
     * @var Application
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function dispatch($controller, $method)
    {
        $controller = $this->app->make($controller);
        $parameters =  $this->resolveClassMethodDependencies($controller, $method);
        return $controller->{$method}(...array_values($parameters));
    }

    protected function resolveClassMethodDependencies($instance, $method): array
    {
        if (! method_exists($instance, $method)) {
            throw new RuntimeException("控制器方法不存在: {$method}");
        }
        $reflector = new ReflectionMethod($instance, $method);
        $parameters = [];
        foreach ($reflector->getParameters() as $key => $parameter) {
            $class = $parameter->getClass();
            $name = $parameter->getName();
            if (empty($class)){
                if (!$parameter->getDefaultValue()){
                    throw new RuntimeException("$name 没有默认值");
                }
                $parameters[$name] = $parameter->getDefaultValue();
            }else{
                $parameters[$name] = $this->app->get($parameter->getClass()->name);
            }
        }
        return $parameters;
    }
}