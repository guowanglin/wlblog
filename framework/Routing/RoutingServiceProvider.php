<?php
declare(strict_types=1);

namespace Gwl\Framework\Routing;

use DI\Container;
use Gwl\Framework\Foundation\Application;
use Gwl\Framework\Support\ServiceProvider;
use function DI\factory;

class RoutingServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->set('router', factory(function (Application $app) {
            return new Router($app);
        }));
        $this->app->set(Router::class, factory(function (Application $app) {
            return $app->get('router');
        }));
    }
}