<?php
declare(strict_types=1);

namespace Gwl\Framework\Event;

use Gwl\Framework\Foundation\Application;
use Gwl\Framework\Support\ServiceProvider;
use League\Event\EventDispatcher;
use function DI\factory;

class EventServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->set('events', factory(function (Application $app){
            return new EventDispatcher();
        }));
    }
}