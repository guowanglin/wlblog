<?php

namespace Gwl\Framework\Database;

use Gwl\Framework\Support\ServiceProvider;
use think\facade\Db;

class DatabaseServiceProvider extends ServiceProvider
{
    public function register()
    {
        Db::setConfig($this->app['config']['database']);
    }
}