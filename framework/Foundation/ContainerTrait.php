<?php

namespace Gwl\Framework\Foundation;

trait ContainerTrait
{
    public function get($name)
    {
        return $this->container->get($name);
    }

    public function set(string $name, $value)
    {
        return $this->container->set($name, $value);
    }

    public function make($name, array $parameters = [])
    {
        return $this->container->make($name, $parameters);
    }

    public function offsetExists($offset)
    {
        return $this->container->has($offset);
    }

    public function offsetGet($offset)
    {
        return $this->container->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        return $this->container->set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        return $this->container->set($offset, null);
    }
}