<?php

declare(strict_types=1);

namespace Gwl\Framework\Foundation\Bootstrap;


use Gwl\Framework\Foundation\Application;

class BootProviders
{
    public function bootstrap(Application $app)
    {
        $app->boot();
    }
}