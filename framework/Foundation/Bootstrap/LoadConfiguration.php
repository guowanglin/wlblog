<?php
declare(strict_types=1);

namespace Gwl\Framework\Foundation\Bootstrap;

use Gwl\Framework\Config\Repository;
use Gwl\Framework\Foundation\Application;
use Symfony\Component\Finder\Finder;

class LoadConfiguration
{
    public function bootstrap(Application $app)
    {
        $configPath = realpath($app->configPath());
        $items = $this->getItems($configPath);
        $app->set("config", $config = new Repository($items));
        date_default_timezone_set($config->get('app.timezone', 'UTC'));
        mb_internal_encoding('UTF-8');
    }

    protected function getItems($configPath)
    {
        $items = [];
        foreach (Finder::create()->files()->name("*.php")->in($configPath) as $file){
            $items[basename($file->getBasename(), '.php')] = require $file->getRealPath();
        }
        return $items;
    }
}