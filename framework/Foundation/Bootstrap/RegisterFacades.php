<?php

declare(strict_types=1);

namespace Gwl\Framework\Foundation\Bootstrap;

use Gwl\Framework\Foundation\Application;
use Gwl\Framework\Support\Facades\Facade;

class RegisterFacades
{
    public function bootstrap(Application $app)
    {
        Facade::setFacadeApplication($app);
    }
}