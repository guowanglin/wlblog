<?php
declare(strict_types=1);

namespace Gwl\Framework\Foundation\Bootstrap;

use Gwl\Framework\Foundation\Application;

class RegisterProviders
{
    public function bootstrap(Application $app)
    {
        $app->registerConfiguredProviders();
    }
}