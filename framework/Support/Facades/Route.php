<?php
declare(strict_types=1);

namespace Gwl\Framework\Support\Facades;

use Gwl\Framework\Facades\Facade;

class Route extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'router';
    }
}