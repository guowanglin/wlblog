<?php
declare(strict_types=1);

namespace Gwl\Framework\Support\Facades;

/**
 * @method static render($name, array $context = []): string
 */
class View extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'view';
    }
}