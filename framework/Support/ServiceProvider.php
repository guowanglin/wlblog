<?php
declare(strict_types=1);

namespace Gwl\Framework\Support;

use Gwl\Framework\Foundation\Application;
use Gwl\Framework\Foundation\ApplicationContext;

abstract class ServiceProvider
{
    /**
     * @var ApplicationContext
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    abstract public function register();
}