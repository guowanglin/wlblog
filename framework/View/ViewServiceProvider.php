<?php
declare(strict_types=1);

namespace Gwl\Framework\View;

use DI\Container;
use Gwl\Framework\Support\ServiceProvider;
use function DI\factory;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->set("view", factory(function (Container $container){
            $loader = new \Twig\Loader\FilesystemLoader($container->get('config')['view.path']);
            $twig = new \Twig\Environment($loader, [
//                'cache' => $container->get('config')['view.compiled'],
            ]);
            return $twig;
        }));
    }
}