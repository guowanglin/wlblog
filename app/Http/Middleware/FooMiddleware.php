<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Request;

class FooMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
    }
}