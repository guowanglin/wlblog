<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Services\HomeService;
use Gwl\Framework\Support\Facades\View;
use Symfony\Component\HttpFoundation\Request;

class Home
{
    public function index(Request $request)
    {
        $page = $request->get('page',1);
        $data = HomeService::homePage($page);
        $updates = HomeService::updates();
        return View::render("index.html",['data' => $data, 'updates' => $updates]);
    }

    public function post(Request $request)
    {
        $data = HomeService::detail($request->get('id'));
        $updates = HomeService::updates();
        return View::render("post.html",['data' => $data, 'updates' => $updates]);
    }

    public function web(Request $request)
    {
        $data = HomeService::web();
        $updates = HomeService::updates();
        return View::render("index.html",['data' => $data, 'updates' => $updates]);
    }

    public function language(Request $request)
    {
        $data = HomeService::language($request);
        $updates = HomeService::updates();
        return View::render("index.html",['data' => $data, 'updates' => $updates]);
    }

    public function ai(Request $request)
    {
        $data = HomeService::ai();
        $updates = HomeService::updates();
        return View::render("index.html",['data' => $data, 'updates' => $updates]);
    }

    public function database(Request $request)
    {
        $data = HomeService::database();
        $updates = HomeService::updates();
        return View::render("index.html",['data' => $data, 'updates' => $updates]);
    }
}