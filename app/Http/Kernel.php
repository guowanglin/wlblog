<?php
declare(strict_types=1);

namespace App\Http;

use App\Http\Middleware\FooMiddleware;

class Kernel extends \Gwl\Framework\Foundation\Http\Kernel
{
    protected $middlewares = [
        FooMiddleware::class
    ];
}