<?php
declare(strict_types=1);

namespace App\Http\Services;

use Symfony\Component\HttpFoundation\Request;
use think\facade\Db;

class HomeService
{
    protected static $instance = null;

    public function homePage($page)
    {
        return Db::table('wl_articles')
            ->field('id,title,remark,tags,created_at,view_num')
            ->order('id','desc')
            ->paginate([
                'list_rows' => 15,
                'page' => $page,
                'path' => '/?page=[PAGE]'
            ]);
    }

    public function detail($id)
    {
        return Db::table('wl_articles')->find($id);
    }

    //最近更新
    public function updates()
    {
        return Db::table('wl_articles')->field('id,title,author')->limit(20)->select();
    }

    public function web()
    {
        return Db::table('wl_articles')
            ->field('id,title,remark,tags,created_at,view_num')
            ->whereLike('tags',"%js%",'or')
            ->whereLike('tags',"%vue%",'or')
            ->whereLike('tags',"%react%",'or')
            ->whereLike('tags',"%javascript%",'or')
            ->whereLike('tags',"%html%",'or')
            ->whereLike('tags',"%css%",'or')
            ->whereLike('tags',"%CSS%",'or')
            ->order('id','desc')
            ->paginate([
                'list_rows' => 15,
                'path' => '/web/[PAGE]'
            ]);
    }

    public function language(Request $request)
    {
        return Db::table('wl_articles')
            ->field('id,title,remark,tags,created_at,view_num')
            ->whereLike('tags',"%php%", "or")
            ->whereLike('tags',"%java%","or")
            ->whereLike('tags',"%.net%","or")
            ->whereLike('tags',"%c#%","or")
            ->whereLike('tags',"%python%","or")
            ->whereLike('tags',"%go%","or")
            ->whereLike('tags',"%golang%","or")
            ->whereLike('tags',"%c++%","or")
            ->order('id','desc')
            ->paginate([
                'list_rows' => 15,
                'path' => '/language/[PAGE]'
            ]);
    }

    public function ai()
    {
        return Db::table('wl_articles')
            ->field('id,title,remark,tags,created_at,view_num')
            ->whereLike('tags',"%ai%","or")
            ->whereLike('tags',"%人工智能%","or")
            ->whereLike('tags',"%Ai%","or")
            ->whereLike('tags',"%机器学习%","or")
            ->whereLike('tags',"%深度学习%","or")
            ->order('id','desc')
            ->paginate([
                'list_rows' => 15,
                'path' => '/language/[PAGE]'
            ]);    }

    public function database()
    {
        return Db::table('wl_articles')
            ->field('id,title,remark,tags,created_at,view_num')
            ->whereLike('tags',"%sql%","or")
            ->whereLike('tags',"%SQL%","or")
            ->whereLike('tags',"%数据库%","or")
            ->order('id','desc')
            ->paginate([
                'list_rows' => 15,
                'path' => '/database/[PAGE]'
            ]);
    }

    public static function __callStatic($method, $arguments)
    {
        if (!static::$instance){
            self::$instance = new self();
        }
        return call_user_func_array([self::$instance, $method], $arguments);
    }
}