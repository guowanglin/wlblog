<?php
declare(strict_types=1);

namespace App\Providers;

use Gwl\Framework\Routing\Router;
use Gwl\Framework\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        $this->app['router']->loadRoutes(require BASE_PATH . "/routes/web.php");
    }
}