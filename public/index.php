<?php

require_once "../vendor/autoload.php";

use \Gwl\Framework\Foundation\ApplicationContext;

define("BASE_PATH", dirname(__DIR__));

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$container = require  __DIR__.'/../bootstrap/app.php';
$app = $container->get("app");
$app->run();
