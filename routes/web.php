<?php

return (function (FastRoute\RouteCollector $r) {
    $r->get("/", "\App\Http\Controllers\Home@index");
    $r->get("/post/{id:\d+}.html", "\App\Http\Controllers\Home@post");
    $r->get("/web", "\App\Http\Controllers\Home@web");
    $r->get("/database", "\App\Http\Controllers\Home@database");
    $r->get("/language", "\App\Http\Controllers\Home@language");
    $r->get("/ai", "\App\Http\Controllers\Home@ai");
});