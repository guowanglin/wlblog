<?php

use DI\ContainerBuilder;

$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions(dirname(__DIR__) . '/config/dependents.php');
$container = $containerBuilder->build();

return $container;