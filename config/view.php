<?php

return [
    'path' => BASE_PATH."/resources/views",
    'compiled' => BASE_PATH."/storage/framework/views",
];