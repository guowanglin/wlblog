<?php

return [
    'timezone' => 'Asia/Shanghai',
    'providers' => [
        \App\Providers\RouteServiceProvider::class,
        \Gwl\Framework\View\ViewServiceProvider::class,
        \Gwl\Framework\Database\DatabaseServiceProvider::class,
    ]
];