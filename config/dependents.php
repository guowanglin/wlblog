<?php


return  [
    "app" => \DI\factory(function (\DI\Container $container){
        return new \Gwl\Framework\Foundation\Application($container);
    }),
    \Gwl\Framework\Foundation\Application::class => \DI\factory(function (\DI\Container $container){
        return $container->get('app');
    }),
    \Gwl\Framework\Foundation\Http\Kernel::class => \DI\factory(function (\DI\Container $container)
    {
        return new \App\Http\Kernel($container->get('app'), $container->get('router'));
    })
];